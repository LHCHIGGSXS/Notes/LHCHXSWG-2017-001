LATEX    = latex
BIBTEX   = bibtex
DVIPS    = dvips

BASENAME = LHCHXSWG-2017-001

default: testpdflatex

testlatex:
	latex  ${BASENAME}
	latex  ${BASENAME}
	bibtex ${BASENAME}
	latex  ${BASENAME}
	latex  ${BASENAME}
	dvips -j0 ${BASENAME}.dvi -o ${BASENAME}.ps
	ps2pdf13 -sPAPERSIZE=a4 ${BASENAME}.ps
#	dvips -o ${BASENAME}.ps ${BASENAME}.dvi  
#	dvipdf -sPAPERSIZE=a4 -dPDFSETTINGS=/prepress ${BASENAME}
#	dvipdf -sPAPERSIZE=letter -dPDFSETTINGS=/prepress ${BASENAME}
#
# commit LaTeX outputs to SVN
#	svn ci -m "update pdf" LHCHXSWG-2012-001.pdf
#	echo "Successfully committed LaTeX outputs to SVN."

testpdflatex:
	pdflatex -draftmode ${BASENAME}
	pdflatex -draftmode ${BASENAME}
	bibtex    ${BASENAME}
	pdflatex -draftmode ${BASENAME}
	pdflatex -synctex=1 ${BASENAME}

arxiv:
	tar czvf ${BASENAME}.tgz ${BASENAME}.{bbl,tex} appendices/ conclusions/ benchmarks/ framework/ intro/ newdirections/ macros.tex lhchiggs.sty cernyrep.cls cernall.sty heppennames2.sty cernchemsym.sty cernunits.sty

#
# standard Latex targets
#

%.dvi:	%.tex 
	$(LATEX) $<

%.bbl:	%.tex *.bib
	$(LATEX) $*
	$(BIBTEX) $*

%.ps:	%.dvi
	$(DVIPS) $< -o $@

%.pdf:	%.tex
	$(PDFLATEX) $<

.PHONY: clean

clean:
	rm -f *.aux *.log *.bbl *.blg *.brf *.cb *.ind *.idx *.ilg *.inx *.dvi *.toc *.out *~ ~* spellTmp 

